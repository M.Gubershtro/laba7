<?php
if (empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW'])) {
header('HTTP/1.1 401 Unanthorized');
header('WWW-Authenticate: Basic realm="For MaksLaba7"');
print('<h1>401 Требуется авторизация</h1>');
exit();
}
include 'db.php';

$login=$_SERVER['PHP_AUTH_USER'];
$request = "SELECT * from admin where login =?";
$result = $db->prepare($request);
$result->execute(array($login));
$flag = 0;
while ($data = $result->fetch()) {
if ($data['login'] == $_SERVER['PHP_AUTH_USER'] && password_verify($_SERVER['PHP_AUTH_PW'],$data['hash'])) {
$flag = 1;
}
}
if ($flag == 0) {
header('HTTP/1.1 401 Unanthorized');
header('WWW-Authenticate: Basic realm="For MaksLaba7"');
print('<h1>401 Требуется авторизация</h1>');
exit();
}