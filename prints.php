<?php
    function print_href($id)
    {
       print '<form action = "delete.php" method="POST">
    <input type="hidden" name="' . $id . '">
    <input type="submit" value="Delete">
    </form>';
    print '<br/>';
    print '<form action = "change.php" method="POST">
    <input type="hidden" name="' . $id . '">
    <input type="submit" value="Change">
    </form>';

    }

    function print_all_info($db,$result,$result_login)
    {
        while (($data = $result->fetch())) {
            $id=$data['id'];
            print '<tr><td>';
            print $data['id'];
            print '</td><td>';
            print strip_tags($data['fio']);
            print '</td><td>';
            print strip_tags($data['email']);
            print '</td><td>';
            print strip_tags($data['birth']);
            print '</td><td>';
            print strip_tags($data['sex']);
            print '</td><td>';
            print strip_tags($data['limb']);
            print '</td><td>';
            print strip_tags($data['about']);
            print '</td><td>';
            $ability_data = ['god', 'wall', 'lev'];
            $ability_name = ['Бессмертие','Сквозь стены','Левитация'];
            $count=0;
            foreach ($ability_data as $ability) {
                if($data[$ability]==1)
                {
                    print $ability_name[$count];
                    print '</br>';
                    $count++;
                }
            }
            print '</td>';
            print '<td>';
            $user=$result_login->fetch();
            print strip_tags($user['login']);
            print '</td><td>';
            print strip_tags($user['hash']);
            print '</td><td>';
            print_href($id);
        }
    }

    function print_table_count($db)
    {
        $request = "SELECT COUNT(god) FROM forma WHERE god=1 GROUP BY god ";
        $result = $db->prepare($request);
        $result->execute();
        $data_god = $result->fetch()[0];
        $request = "SELECT COUNT(wall) FROM forma WHERE wall=1 GROUP BY wall ";
        $result = $db->prepare($request);
        $result->execute();
        $data_wall = $result->fetch()[0];
        $request = "SELECT COUNT(lev) FROM forma WHERE lev=1 GROUP BY lev";
        $result = $db->prepare($request);
        $result->execute();
        $data_lev = $result->fetch()[0];

        print '<h2>Статистика по сверхспособностям:</h2>';
        print '<table class="table">';
        print '
                            <tr>
                            <th>Бессмертие</th>
                            <th>Cквозь стены</th>
                            <th>Левитация</th>
                            </tr>';
        print '<tr><td>';
        print $data_god;
        print '</td><td>';
        print $data_wall;
        print '</td><td>';
        print $data_lev;
        print '</td></tr>';
        print '</table>';
    }
