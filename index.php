<?php
/**
 * Реализовать возможность входа с паролем и логином с использованием
 * сессии для изменения отправленных данных в предыдущей задаче,
 * пароль и логин генерируются автоматически при первоначальной отправке формы.
 */
function generate($number)
{
    $arr = array('a','b','c','d','e','f',
        'g','h','i','j','k','l',
        'm','n','o','p','r','s',
        't','u','v','x','y','z',
        'A','B','C','D','E','F',
        'G','H','I','J','K','L',
        'M','N','O','P','R','S',
        'T','U','V','X','Y','Z',
        '1','2','3','4','5','6',
        '7','8','9','0');
// Генерируем пароль
    $pass = "";
    for($i = 0; $i < $number; $i++)
    {
// Вычисляем случайный индекс массива
        $index = rand(0, count($arr) - 1);
        $pass .= $arr[$index];
    }
    return $pass;
}
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Массив для временного хранения сообщений пользователю.
    print '<a href="admin.php">Войти как администратор</a>';
    $messages = array();


    // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
    // Выдаем сообщение об успешном сохранении.
    if (!empty($_COOKIE['save'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('save', '', 100000);
        setcookie('login', '', 100000);
        setcookie('pass', '', 100000);
        // Если есть параметр save, то выводим сообщение пользователю.
        $messages[] = 'Спасибо, результаты сохранены.';
        if($_COOKIE['admin']=='1'){
            header('Location:login.php');
        }

        if (!empty($_COOKIE['pass'])) {
            $messages[] = sprintf('<br/> <a href="login.php">войти</a> в аккаунт Логин : <strong>%s</strong>
        <br/> Пароль : <strong>%s</strong> ',
                strip_tags($_COOKIE['login']),
                strip_tags($_COOKIE['pass']));
        }
    }

    // Складываем признак ошибок в массив.
    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['sex'] = !empty($_COOKIE['sex_error']);
    $errors['bio'] = !empty($_COOKIE['bio_error']);
    $errors['check'] = !empty($_COOKIE['check_error']);
    $errors['abil'] = !empty($_COOKIE['abil_error']);
    $errors['year'] = !empty($_COOKIE['year_error']);
    $errors['limb'] = !empty($_COOKIE['limb_error']);
    // TODO: аналогично все поля.

    // Выдаем сообщения об ошибках.
    if ($errors['fio']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('fio_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Заполните имя корректно </div>';
    }
    if ($errors['email']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('email_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Заполните почту </div>';
    }
    if ($errors['sex']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('sex_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Выберите пол </div>';
    }
    if ($errors['bio']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('bio_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Введите биографию </div>';
    }
    if ($errors['check']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('check_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Подтвердите согласие </div>';
    }
    if ($errors['abil']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('abil_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Выберите сверхспособность </div>';
    }
    if ($errors['year']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('year_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Корректно введите дату рождения   </div>';
    }
    if ($errors['limb']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('limb_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Выберите количество конечностей   </div>';
    }


    // TODO: тут выдать сообщения об ошибках в других полях.
    include 'db.php';
    // Складываем предыдущие значения полей в массив, если есть.
    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
    $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
    $values['sex_value'] = empty($_COOKIE['sex_value']) ? '' : strip_tags($_COOKIE['sex_value']);
    $values['bio_value'] = empty($_COOKIE['bio_value']) ? '' : strip_tags($_COOKIE['bio_value']);
    $values['check_value'] = empty($_COOKIE['check_value']) ? '' : strip_tags($_COOKIE['check_value']);
    $values['abil_value'] = empty($_COOKIE['abil_cookie']) ? '' : unserialize(strip_tags($_COOKIE['abil_cookie']));
    $values['year_value'] = empty($_COOKIE['year_value']) ? '' : strip_tags($_COOKIE['year_value']);
    $values['limb_value'] = empty($_COOKIE['limb_value']) ? '' : strip_tags($_COOKIE['limb_value']);
    $values['abil_value0']=empty($values['abil_value'][0]) ? '' :strip_tags($values['abil_value'][0]);
    $values['abil_value1']=empty($values['abil_value'][1]) ? '' :strip_tags($values['abil_value'][1]);
    $values['abil_value2']=empty($values['abil_value'][2]) ? '' :strip_tags($values['abil_value'][2]);

    // TODO: аналогично все поля.
    $flag = 0;
    foreach($errors as $err){ //Проверка ошибок
        if($err==1)$flag=1;
    }

    if (!$flag&&!empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {
        $login = $_SESSION['login'];
        $stmt = $db->prepare("SELECT id FROM users WHERE login = ?");
        $stmt->execute(array($login));
        $id='';
        while($row = $stmt->fetch())
        {
            $id=$row['id'];
        }

        $request = "SELECT fio,email,birth,sex,limb,about,checkbox FROM forma WHERE id = ?";
        $result = $db -> prepare($request);
        $result ->execute(array($id));

        $data = $result->fetch(PDO::FETCH_ASSOC);

        $values['fio'] = strip_tags($data['fio']);
        $values['email'] = strip_tags($data['email']);
        $values['year_value'] = strip_tags($data['birth']);
        $values['sex_value'] = strip_tags($data['sex']);
        $values['limb_value'] = strip_tags($data['limb']);
        $values['bio_value'] = strip_tags($data['about']);
        $values['check_value'] = strip_tags($data['checkbox']);



        $login_ses=strip_tags($_SESSION['login']);
        $uid_ses=strip_tags($_SESSION['uid']);

        echo '
<label style="text-align: center"> Вход выполнен : <br/><strong>Логин</strong>:'.$login_ses.' <br/><strong>Ваш id этой сессии</strong>: '.$uid_ses.' </label>
';

        print '
            <a href="login.php">Выйти</a>
';
    }
    else{

        print '
            <a href="login.php">Авторизация</a>
        ';
    }

    // Включаем содержимое файла form.php.
    // В нем будут доступны переменные $messages, $errors и $values для вывода
    // сообщений, полей с ранее заполненными данными и признаками ошибок.
    include('form.php');
} else {
    if (!isset($_COOKIE['admin'])) {
        setcookie('admin', '0');
    }


    // Проверяем ошибки.
    $errors = FALSE;
    if (empty($_POST['fio']) || !(preg_match("/^[А-ЯA-Z][а-яa-zА-ЯA-Z\-]{0,}\s[А-ЯA-Z][а-яa-zА-ЯA-Z\-]{1,}(\s[А-ЯA-Z][а-яa-zА-ЯA-Z\-]{1,})?$/u", $_POST['fio'])))
    {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('fio_value', $_POST['fio'], time() + 365 * 24 * 60 * 60);
    }
    if (empty($_POST['email'])|| !preg_match('/^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u', $_POST['email'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
    }
    if (!isset($_POST['radio2'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('sex_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('sex_value', $_POST['radio2'], time() + 365 * 24 * 60 * 60);
    }
    if (empty($_POST['textarea1'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('bio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('bio_value', $_POST['textarea1'], time() + 365 * 24 * 60 * 60);
    }
    if (!isset($_POST['checkbox'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('check_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('check_value', $_POST['checkbox'], time() + 365 * 24 * 60 * 60);
    }
    $abil=0;
    $ability_data = ['god', 'wall', 'lev'];
    $abilities = $_POST['select1'];
    foreach ($ability_data as $ability) {
        if(in_array($ability, $abilities))
        {
            $abil=1;
        }
    }
    if(!$abil)
    {
        setcookie('abil_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('abil_cookie', serialize($_POST['select1']), time() + 365 * 24 * 60 * 60);
    }

    if (!isset($_POST['radio1'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('limb_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('limb_value', $_POST['radio1'], time() + 365 * 24 * 60 * 60);
    }
    if (empty($_POST['birthyear']) || $_POST['birthyear'] < 1910 || $_POST['birthyear'] > 2021) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('year_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('year_value', $_POST['birthyear'], time() + 365 * 24 * 60 * 60);
    }


    if ($errors) {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        header('Location: index.php');
        exit();
    } else {
        include 'db.php';
        //Удаляем Cookies с признаками ошибок.
        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('sex_error', '', 100000);
        setcookie('bio_error', '', 100000);
        setcookie('check_error', '', 100000);
        setcookie('abil_error', '', 100000);
        setcookie('year_error', '', 100000);
        setcookie('limb_error', '', 100000);
        //удалить остальные Cookies.
        //
        if (!empty($_COOKIE[session_name()]) &&
            session_start() && !empty($_SESSION['login'])&&!empty($_SESSION['uid'])) {

            $fio = $_POST['fio'];
            $email = $_POST['email'];
            $birthyear = $_POST['birthyear'];
            $sex = $_POST['radio2'];
            $limb = $_POST['radio1'];
            $bio = $_POST['textarea1'];

            $ability_data = ['god', 'wall', 'lev'];
            $abilities = $_POST['select1'];
            $ability_insert = [];
            foreach ($ability_data as $ability) {
                $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;

            }
            $god = $ability_insert['god'];
            $wall = $ability_insert['wall'];
            $lev = $ability_insert['lev'];

            $login = $_SESSION['login'];
            $stmt = $db->prepare("SELECT id FROM users WHERE login = ?");
            $stmt->execute(array($login));
            $id='';
            while($row = $stmt->fetch())
            {
                $id=$row['id'];
            }

            $sql = "UPDATE forma SET fio= ?,email= ?,birth= ?,sex= ?,limb= ?,about= ?,god='$god',wall='$wall',lev='$lev' WHERE id= ?";
            $stmt = $db->prepare($sql);
            $stmt->execute(array($fio,$email,$birthyear,$sex,$limb,$bio,$id));

        }
        else {
            // Генерируем уникальный логин и пароль.
            // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
            $login=generate(rand(1,25));

            $pass =generate(rand(1,25));

            $hash_pass=password_hash($pass, PASSWORD_DEFAULT);
            // Сохраняем в Cookies.
            setcookie('login', $login);
            setcookie('pass', $pass);

            // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
            // complete

            $fio = $_POST['fio'];
            $email = $_POST['email'];
            $birthyear = $_POST['birthyear'];
            $sex = $_POST['radio2'];
            $limb = $_POST['radio1'];
            $bio = $_POST['textarea1'];
            $c_b= $_POST['checkbox'];
            include 'db.php';

            $ability_data = ['god', 'wall', 'lev'];
            $abilities = $_POST['select1'];
            $ability_insert = [];
            foreach ($ability_data as $ability) {
                $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;
            }

            $stmt = $db->prepare("INSERT INTO forma (fio, birth,email,sex,limb,god,wall,lev,about,checkbox) VALUES (:fio, :birth,:email,:sex,:limb,:god,:wall,:lev,:about,:checkbox)");
            $stmt->execute(array('fio' => $fio, 'birth' => $birthyear, 'email' => $email, 'sex' => $sex, 'limb' => $limb, 'god' => $ability_insert['god'], 'wall' => $ability_insert['wall'], 'lev' => $ability_insert['lev'], 'about' => $_POST['textarea1'], 'checkbox' => $_POST['checkbox']));


            $stmt = $db->prepare("INSERT INTO users (login, hash) VALUES (:login,:hash)");
            $stmt->execute(array('login'=>$login,'hash'=>$hash_pass));


        }
        setcookie('save', '1');
        header('Location: index.php');
    }
}
